using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Scr_PlaceObject : MonoBehaviour
{
    [SerializeField] GameObject hologramContainer;
    [SerializeField] GameObject[] objectHologram;
    [SerializeField] GameObject[] prefabObject;
    [SerializeField] RawImage[] inventoryImages;
    [SerializeField] Material[] hologramMaterial;

    RaycastHit hit;
    public static bool isActive;
    float mouseWheelRotatio;
    [Range(0, 2)] int activeObject;
    int materialIndex;
    KeyCode activeKey = KeyCode.E;

    void Start()
    {
        isActive = false;
        activeObject = 0;
    }
    void Update()
    {
        ChangeActiveState();

        SelectObject();

        PlaceObject(isActive);
    }
    void ChangeActiveState()
    {
        if (Input.GetKeyDown(activeKey))
        {
            isActive = !isActive;
        }
    }
    void PlaceObject(bool _isActive)
    {
        if (!_isActive)
        {
            objectHologram[activeObject].SetActive(false);
            return;
        }

        objectHologram[activeObject].SetActive(true);

        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
        {
            objectHologram[activeObject].transform.position = hit.point;
            objectHologram[activeObject].transform.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal);

            RotateObject();

            if (Input.GetMouseButtonDown(0) && Scr_GlobalStats.wood > 0)
            {
                Scr_GlobalStats.wood--;

                GameObject objectClone = Instantiate(prefabObject[activeObject], objectHologram[activeObject].transform.position, objectHologram[activeObject].transform.rotation);
                PlayPlaceAnimation(objectClone.transform);
            }
        }
    }
    void RotateObject()
    {
        mouseWheelRotatio += Input.mouseScrollDelta.y;
        objectHologram[activeObject].transform.Rotate(Vector3.up, mouseWheelRotatio * 10f);
    }
    void PlayPlaceAnimation(Transform _transform)
    {
        Vector3 originalScale = _transform.localScale;
        Vector3 scallingFactor = new Vector3(0.2f, 0.2f, 0.2f);

        Sequence mySequence = DOTween.Sequence();

        mySequence.Append(_transform.DOScale(originalScale + scallingFactor, 0.3f));

        mySequence.Append(_transform.DOScale(originalScale, 0.2f));
    }
    void SelectObject()
    {
        if (!isActive)
            return;

        if (Input.GetKeyDown(KeyCode.Alpha1))
            activeObject = 0;
        if (Input.GetKeyDown(KeyCode.Alpha2))
            activeObject = 1;
        if (Input.GetKeyDown(KeyCode.Alpha3))
            activeObject = 2;

        MaterialManager();
        ItemManager();
    }
    void ItemManager()
    {
        for (int i = 0; i < objectHologram.Length; i++)
        {
            if (activeObject == i)
            {
                objectHologram[i].SetActive(true);
                inventoryImages[i].color = Color.blue;
                objectHologram[i].GetComponent<MeshRenderer>().material = hologramMaterial[materialIndex];
            }
            else
            {
                objectHologram[i].SetActive(false);
                inventoryImages[i].color = Color.white;
            }
        }
    }
    void MaterialManager()
    {
        if (Scr_GlobalStats.wood > 0)
            materialIndex = 0;
        else
            materialIndex = 1;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Scr_Axe : MonoBehaviour
{
    MeshRenderer axeRenderer;

    void Start()
    {
        axeRenderer = GetComponent<MeshRenderer>();
    }
    void Update()
    {
        AxeChop();
    }

    private void AxeChop()
    {
        if (Scr_PlaceObject.isActive)
        {
            axeRenderer.enabled = false;
            return;
        }
        else
        {
            axeRenderer.enabled = true;
        }

        if (Input.GetMouseButtonDown(0)) 
        {
            Sequence mySequence = DOTween.Sequence();

            mySequence.Append(transform.DOLocalRotate(new Vector3(0, -120f, -45f), 0.2f));

            mySequence.Append(transform.DOLocalRotate(new Vector3(0, -120f, -15f), 0.2f));
        }
    }
}

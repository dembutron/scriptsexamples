using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_PickUpObject : MonoBehaviour
{
    [SerializeField] string objectTag;

    RaycastHit hit;

    void Update()
    {
        PickUp();
    }
    void PickUp()
    {
        if (Scr_PlaceObject.isActive)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
            {
                if (hit.transform.tag != objectTag)
                    return;

                Scr_GlobalStats.wood++;
                Destroy(hit.transform.gameObject, 0.1f);
            }
        }
    }
}

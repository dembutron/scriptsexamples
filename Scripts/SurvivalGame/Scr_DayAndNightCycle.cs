using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_DayAndNightCycle : MonoBehaviour
{
    [SerializeField] float speed = 1f;

    float SunRotation;

    void Update()
    {
        SunRotation = Time.deltaTime * speed;
        transform.Rotate(Vector3.right * SunRotation);
    }

}

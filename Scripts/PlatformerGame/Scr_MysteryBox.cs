using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_MysteryBox : MonoBehaviour
{
    [SerializeField] GameObject itemStatic;
    [SerializeField] GameObject itemDynamic;
    [SerializeField] Material initialMaterial;
    [SerializeField] Material secondMaterial;

    MeshRenderer boxRenderer;

    bool isActive = false;
    void Start()
    {
        boxRenderer = GetComponent<MeshRenderer>();
        boxRenderer.material = initialMaterial;
    }
    void Update()
    {
        PullOutObject();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !isActive)
        {
            itemStatic.SetActive(true);
            isActive = true;
            boxRenderer.material = secondMaterial;
        }
    }
    void PullOutObject()
    {
        if (isActive)
        {
            if (itemStatic.transform.position.y <= 6.28f)
            {
                itemStatic.transform.position += new Vector3(0f, 1f, 0f) * Time.deltaTime;
            }
            if (itemStatic.transform.position.y > 6.28f) 
            {
                StartCoroutine(ChangeItem());
            }
        }
    }
    IEnumerator ChangeItem()
    {
        yield return new WaitForSeconds(0.5f);
        if (itemDynamic != null)
        {
            itemStatic.SetActive(false);
            itemDynamic.SetActive(true);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_DefaultBlock : MonoBehaviour
{
    float posX, posY, posZ;

    [SerializeField] bool isActive = false;
    [SerializeField] bool isPlayerBig = false;

    Scr_PlayerMovement player;
    void Start()
    {
        posX = transform.position.x;
        posY = transform.position.y;
        posZ = transform.position.z;

        player = FindObjectOfType<Scr_PlayerMovement>();
    }
    void Update()
    {
        if (player != null)
            isPlayerBig = player.isBig;

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !isActive)
        {
            isActive = true;
            CheckStatus();
        }
    }
    void CheckStatus()
    {
        if (isPlayerBig)
            StartCoroutine(DestroyBlock());
        else
            StartCoroutine(HitBlock());
    }
    IEnumerator HitBlock()
    {
        transform.position = new Vector3(posX, posY + 0.25f, posZ);
        yield return new WaitForSeconds(0.25f);
        transform.position = new Vector3(posX, posY, posZ);
        yield return new WaitForSeconds(0.25f);
        isActive = false;
    }
    IEnumerator DestroyBlock()
    {
        transform.position = new Vector3(posX, posY + 0.25f, posZ);
        yield return new WaitForSeconds(0.125f);
        Destroy(gameObject);
        isActive = false;
    }
}

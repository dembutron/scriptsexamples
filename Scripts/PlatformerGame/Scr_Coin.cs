using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Scr_Coin : MonoBehaviour
{
    [SerializeField] float rotationSpeed = 0.5f;

    MeshRenderer coinRenderer;
    Collider coinCollider;

    Vector3 rotationVector;
    void Start()
    {
        coinRenderer = GetComponent<MeshRenderer>();
        coinCollider = GetComponent<Collider>();
        coinRenderer.enabled = true;
        coinCollider.enabled = true;

        rotationVector = Vector3.forward * -rotationSpeed;
    }
    void Update()
    {
        transform.Rotate(rotationVector, Space.Self);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Scr_GameManager.score++;

            coinRenderer.enabled = false;
            coinCollider.enabled = false;

            Destroy(gameObject, 1f);
        }
    }
}

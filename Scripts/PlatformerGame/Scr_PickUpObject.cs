using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class Scr_PickUpObject : MonoBehaviour
{
    [SerializeField] Transform groundCheck;
    [SerializeField] LayerMask groundMask;

    Rigidbody objectRB;

    Vector3 direction = new Vector3(1f, 0f, 0f);
    public bool goRight = true;
    public bool isGrounded = false;
    public bool isActive = false;
    public float speed = 10f;
    float groundDistance = 0.2f;
    void Start()
    {
        objectRB = GetComponent<Rigidbody>();
    }
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        ObjectMove();
    }

    void ObjectMove()
    {
        if (!isActive)
            return;

        if (goRight)
            direction.x = 1f;
        else
            direction.x = -1f;

        if (isGrounded)
        {
            objectRB.velocity = direction * speed;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 9)
            goRight = !goRight;

        if (collision.gameObject.tag == "Player")
        {
            StartCoroutine(DestroySequence());
        }
    }
    IEnumerator DestroySequence()
    {
        SphereCollider objectCollider = GetComponent<SphereCollider>();

        transform.Find("Body1").GetComponent<MeshRenderer>().enabled = false;
        transform.Find("Body2").GetComponent<MeshRenderer>().enabled = false;

        objectCollider.enabled = false;

        yield return new WaitForSeconds(1f);

        Destroy(gameObject);
    }
}

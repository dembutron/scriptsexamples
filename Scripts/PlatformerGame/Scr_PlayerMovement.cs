using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class Scr_PlayerMovement : MonoBehaviour
{
    [SerializeField] float movementSpeed = 10f;
    [SerializeField] float jumpForce = 10f;
    [SerializeField] Vector3 jumpVector;
    [SerializeField] Transform groundCheck;
    [SerializeField] Transform spawnPoint;
    [SerializeField] Transform playerBody;
    [SerializeField] LayerMask groundMask;
    [SerializeField] Animator playerAnimator;

    Rigidbody playerRigidbody;

    float groundDistance = 0.4f;
    float translation = 0f;
    public bool isBig = false;
    bool isGrounded;
    bool isRunning;
    Vector3 moveDirection;
    void Start()
    {
        playerRigidbody = GetComponent<Rigidbody>();
    }
    void Update()
    {
        GroundCheck();

        Jumping();

        //Animation();
    }
    void FixedUpdate()
    {
        Movement();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 10)
            PlayerDie();
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
            PlayerDie();
    }
    void GroundCheck()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
    }
    void Jumping()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            playerRigidbody.AddForce(jumpVector * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }
    }
    void Movement()
    {
        translation = Input.GetAxis("Horizontal") * movementSpeed;

        ChangeConstraints();

        ChangeRotation();

        Animation();

        transform.Translate(translation * Time.deltaTime, 0, 0);
    }
    void Movement(Vector3 direction)
    {
        playerRigidbody.MovePosition(transform.position + (direction * movementSpeed * Time.deltaTime));
    }
    void PlayerDie()
    {
        transform.position = spawnPoint.position;
    }
    void Animation()
    {
        if (playerAnimator == null)
            return;

        if (translation != 0f && !isRunning && isGrounded) 
        {
            playerAnimator.Play("Run In Place");
            isRunning = true;
        }
        if (translation == 0f && isGrounded)
        {
            playerAnimator.Play("Idle");
            isRunning = false;
        }
        if (!isGrounded)
        {
            playerAnimator.Play("Idle");
            isRunning = false;
        }
    }
    void ChangeConstraints()
    {
        if (translation == 0f)
            playerRigidbody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
        else
        {
            playerRigidbody.constraints = RigidbodyConstraints.None;
            playerRigidbody.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
        }
    }
    void ChangeRotation()
    {
        if (translation > 0f)
            playerBody.rotation = new Quaternion(0f, 0f, 0f, 0f);

        if (translation < 0f)
        {
            playerBody.rotation = new Quaternion(0f, 90f, 0f, 0f);
            translation = -translation;
        }
    }
}

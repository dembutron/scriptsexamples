using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class Scr_MovingEnemy : MonoBehaviour
{
    [SerializeField] Transform groundCheck;
    [SerializeField] Transform body;
    [SerializeField] LayerMask groundMask;

    Rigidbody objectRB;

    Vector3 direction = new Vector3(1f, 0f, 0f);
    public bool goRight = true;
    public bool isGrounded = false;
    public bool isActive = false;
    public float speed = 10f;
    float groundDistance = 0.2f;
    void Start()
    {
        objectRB = GetComponent<Rigidbody>();
    }
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        ObjectMove();
    }

    void ObjectMove()
    {
        if (!isActive)
            return;

        if (goRight)
        {
            direction.x = 1f;
            body.rotation = new Quaternion(0f, 0f, 0f, 0f);
        }
        else
        {
            direction.x = -1f;
            body.rotation = new Quaternion(0f, 180f, 0f, 0f);
        }

        if (isGrounded)
        {
            objectRB.velocity = direction * speed;
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 9)
            goRight = !goRight;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GetComponent<Collider>().isTrigger = true;
            Destroy(gameObject);
        }
    }
}

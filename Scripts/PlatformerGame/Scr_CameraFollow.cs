using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_CameraFollow : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] float smoothSpeed = 1.25f;
    [SerializeField] Vector3 offset;

    Vector3 desiredPosition;
    Vector3 smoothedPosition;
    Vector3 targetPositionX;
    void FixedUpdate()
    {
        targetPositionX = new Vector3(target.position.x, 0f, 0f);
        desiredPosition = targetPositionX + offset;
        smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime);

        transform.position = smoothedPosition;
        //transform.LookAt(target);
    }
}

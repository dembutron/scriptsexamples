using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Pathfinder : MonoBehaviour
{
    [SerializeField] Vector2Int startCoordinates;
    public Vector2Int StartCoordinates { get { return startCoordinates; } }

    [SerializeField] Vector2Int destinationCoordinates;
    public Vector2Int DestinationCoordinates { get { return destinationCoordinates; } }

    Scr_Node startNode;
    Scr_Node destinationNode;
    Scr_Node currentSearchNode;

    Queue<Scr_Node> frontier = new Queue<Scr_Node>();
    Dictionary<Vector2Int, Scr_Node> reached = new Dictionary<Vector2Int, Scr_Node>();

    Vector2Int[] directions = { Vector2Int.right, Vector2Int.down, Vector2Int.left, Vector2Int.up };
    Scr_GridManager gridManager;
    Dictionary<Vector2Int, Scr_Node> grid = new Dictionary<Vector2Int, Scr_Node>();
    void Awake()
    {
        gridManager = FindObjectOfType<Scr_GridManager>();
        if (gridManager != null)
        {
            grid = gridManager.Grid;
            startNode = grid[startCoordinates];
            destinationNode = grid[destinationCoordinates];
        }
    }
    void Start()
    {
        GetNewPath();
    }
    public List<Scr_Node> GetNewPath()
    {
        return GetNewPath(startCoordinates);
    }
    public List<Scr_Node> GetNewPath(Vector2Int coordinates)
    {
        gridManager.ResetNodes();
        BreadtheFirstSearch(coordinates);
        return BuildPath();
    }
    void ExploreNeighbors()
    {
        List<Scr_Node> neighbors = new List<Scr_Node>();

        foreach (Vector2Int direction in directions)
        {
            Vector2Int neighborCoords = currentSearchNode.coordinates + direction;

            if (grid.ContainsKey(neighborCoords))
            {
                neighbors.Add(grid[neighborCoords]);
            }
        }

        foreach (Scr_Node neighbor in neighbors)
        {
            if (!reached.ContainsKey(neighbor.coordinates) && neighbor.isWalkable) 
            {
                neighbor.connectedTo = currentSearchNode;
                reached.Add(neighbor.coordinates, neighbor);
                frontier.Enqueue(neighbor);
            }
        }
    }
    void BreadtheFirstSearch(Vector2Int coordinates)
    {
        startNode.isWalkable = true;
        destinationNode.isWalkable = true;

        frontier.Clear();
        reached.Clear();

        bool isRunning = true;

        frontier.Enqueue(grid[coordinates]);
        reached.Add(coordinates, grid[coordinates]);

        while (frontier.Count > 0 && isRunning)
        {
            currentSearchNode = frontier.Dequeue();
            currentSearchNode.isExplored = true;
            ExploreNeighbors();
            if (currentSearchNode.coordinates == destinationCoordinates) 
            {
                isRunning = false;
            }
        }
    }
    List<Scr_Node> BuildPath()
    {
        List<Scr_Node> path = new List<Scr_Node>();
        Scr_Node currentNode = destinationNode;

        path.Add(currentNode);
        currentNode.isPath = true;

        while (currentNode.connectedTo != null)
        {
            currentNode = currentNode.connectedTo;
            path.Add(currentNode);
            currentNode.isPath = true;
        }

        path.Reverse();

        return path;
    }

    public bool WillBlockPath(Vector2Int coordinates)
    {
        if(grid.ContainsKey(coordinates))
        {
            bool previousState = grid[coordinates].isWalkable;

            grid[coordinates].isWalkable = false;
            List<Scr_Node> newPath = GetNewPath();
            grid[coordinates].isWalkable = previousState;

            if (newPath.Count <= 1)
            {
                GetNewPath();
                return true;
            }
        }

        return false;
    }
    public void NotifyReceivers()
    {
        BroadcastMessage("RecalculatePath", false, SendMessageOptions.DontRequireReceiver);
    }
}

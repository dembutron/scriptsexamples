using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Scr_Node
{
    public Vector2Int coordinates;
    public bool isWalkable;
    public bool isExplored;
    public bool isPath;
    public Scr_Node connectedTo;

    public Scr_Node(Vector2Int _coordinates, bool _isWalkable)
    {
        coordinates = _coordinates;
        isWalkable = _isWalkable;
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Tower : MonoBehaviour
{
    [SerializeField] int cost = 75;
    [SerializeField] float buildDelay = 1f;

    void Start()
    {
        StartCoroutine(Build());
    }


    public bool CreateTower(Scr_Tower tower, Vector3 position)
    {
        Scr_Bank bank = FindObjectOfType<Scr_Bank>();

        if (bank == null)
            return false;

        if (bank.CurrentBalance >= cost)
        {
            bank.Withdraw(cost);
            Instantiate(tower, position, Quaternion.identity);
            return true;
        }

        return false;
    }
    IEnumerator Build()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
            foreach (Transform grandchild in child)
            {
                grandchild.gameObject.SetActive(false);
            }
        }

        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(true);
            yield return new WaitForSeconds(buildDelay);
            foreach (Transform grandchild in child)
            {
                grandchild.gameObject.SetActive(true);
            }
        }
    }
}

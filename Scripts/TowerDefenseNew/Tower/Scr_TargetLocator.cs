using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_TargetLocator : MonoBehaviour
{
    [SerializeField] Transform weapon;
    [SerializeField] ParticleSystem projectileParticles;
    [SerializeField] float range = 15f;

    Transform target;
    Vector3 lookAtVector;
    void Update()
    {
        FindClosestTarget();
        AimWeapon();
    }
    void FindClosestTarget()
    {
        Scr_Enemy[] enemies = FindObjectsOfType<Scr_Enemy>();
        Transform closestTarget = null;
        float maxDistance = Mathf.Infinity;

        foreach (Scr_Enemy enemy in enemies)
        {
            float currentDistance = Vector3.Distance(transform.position, enemy.transform.position);

            if (maxDistance > currentDistance)
            {
                maxDistance = currentDistance;
                closestTarget = enemy.transform;
            }
        }
        target = closestTarget;
    }
    void AimWeapon()
    {
        if (target == null)
            return;

        float targetDistance = Vector3.Distance(transform.position, target.position);

        lookAtVector.Set(target.position.x, weapon.position.y, target.position.z);
        weapon.LookAt(lookAtVector);

        if (targetDistance < range)
            Attack(true);
        else
            Attack(false);
    }
    void Attack(bool isActive)
    {
        var emissionModule = projectileParticles.emission;
        emissionModule.enabled = isActive;
    }
}

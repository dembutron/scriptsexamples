using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Enemy : MonoBehaviour
{
    [SerializeField] int goldReward = 25;
    [SerializeField] int goldPenalty = 25;

    Scr_Bank bank;
    private void Start()
    {
        bank = FindObjectOfType<Scr_Bank>();
    }
    public void RewardGold()
    {
        if (bank == null)
            return;

        bank.Deposit(goldReward);
    }
    public void StealGold()
    {
        if (bank == null)
            return;

        bank.Withdraw(goldPenalty);
    }
}

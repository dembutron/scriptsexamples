using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_DoorDefault : MonoBehaviour
{
    [SerializeField] Transform doorLeaf;
    [SerializeField] float speed = 1f;
    [SerializeField] AudioSource doorAudioSource;

    public bool isActive = false;

    BoxCollider doorBoxCollider;
    Vector3 targetPosition;
    void Start()
    {
        doorLeaf.localPosition = Vector3.zero;
        targetPosition = new Vector3(-3.3f, 0f, 0f);
        doorBoxCollider = GetComponent<BoxCollider>();
    }
    void Update()
    {
        OpenCloseDoor();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            doorAudioSource.Play();
            isActive = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
            StartCoroutine(ClosingDoor());
    }
    void OpenCloseDoor()
    {
        if (isActive)
            doorLeaf.localPosition = Vector3.MoveTowards(doorLeaf.localPosition, targetPosition, speed * Time.deltaTime);
        if (!isActive)
            doorLeaf.localPosition = Vector3.MoveTowards(doorLeaf.localPosition, Vector3.zero, speed * Time.deltaTime);
    }
    IEnumerator ClosingDoor()
    {
        doorBoxCollider.enabled = false;

        yield return new WaitForSeconds(3f);

        doorAudioSource.Play();
        isActive = false;
        doorBoxCollider.enabled = true;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_EnemySoldierAI : MonoBehaviour
{
    [SerializeField] Animator soldierAnimator;
    [SerializeField] AudioSource fireSound;
    [SerializeField] GameObject hurtFlush;

    bool isPlayer = false;
    bool isFiring = false;
    string hitTag;
    RaycastHit hit;
    float fireRate = 1f;

    void Start()
    {
        soldierAnimator.Play("Idle");
        hurtFlush.SetActive(false);
        isPlayer = false;
        isFiring = false;
    }
    void Update()
    {
        SoldierRaycast();
        CheckTarget();
    }
    void SoldierRaycast()
    {
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit))
            hitTag = hit.transform.tag;
    }
    void CheckTarget()
    {
        if (hitTag == "Player" && !isFiring)
            StartCoroutine(EnemyFire());

        if (hitTag != "Player")
        {
            isPlayer = false;
            soldierAnimator.Play("Idle");
        }
    }
    IEnumerator EnemyFire()
    {
        isFiring = true;
        isPlayer = true;
        hurtFlush.SetActive(true);

        Scr_GlobalStats.healthValue -= 5f;

        fireSound.Play();
        soldierAnimator.Play("Shoot", -1, 0);
        soldierAnimator.Play("Shoot");

        yield return new WaitForSeconds(0.2f);
        hurtFlush.SetActive(false);

        yield return new WaitForSeconds(fireRate);
        isFiring = false;
    }
}

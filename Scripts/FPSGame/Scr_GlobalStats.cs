using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Scr_GlobalStats : MonoBehaviour
{
    public static float ammoAmount = 0;
    public static float healthValue;
    public static float livesValue = 3;
    public static float score = 0;
    public static bool haveGun;

    [SerializeField] Text ammoDisplay;
    [SerializeField] Text healthDisplay;
    [SerializeField] Text livesDisplay;
    [SerializeField] Text pickUpDisplay;
    [SerializeField] Text scoreDisplay;
    void Start()
    {
        healthValue = 100;
    }
    void Update()
    {
        TextUpdate();
        CheckHealth();
    }
    void TextUpdate()
    {
        ammoDisplay.text = ammoAmount.ToString();
        healthDisplay.text = healthValue.ToString();
        livesDisplay.text = livesValue.ToString();
        scoreDisplay.text = score.ToString();
    }
    void CheckHealth()
    {
        if (healthValue <= 0f)
            SceneManager.LoadScene(0);
    }
}

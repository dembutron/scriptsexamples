using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scr_PickUpGold : MonoBehaviour
{
    [SerializeField] AudioSource pickUpGoldAudioSource;
    [SerializeField] GameObject pickUpDisplay;
    [SerializeField] MeshRenderer goldRenderer;
    void Update()
    {
        GoldRotate();
    }
    void OnTriggerEnter(Collider other)
    {
        PickUpGold(other);
    }
    void GoldRotate()
    {
        transform.Rotate(new Vector3(0f, 100f * Time.deltaTime, 0f));
    }
    void PickUpGold(Collider other)
    {
        if (other.tag == "Player")
        {
            GetComponent<BoxCollider>().enabled = false;
            pickUpDisplay.SetActive(false);
            pickUpDisplay.GetComponent<Text>().text = "Gold Bar";
            pickUpDisplay.SetActive(true);
            Scr_GlobalStats.score += 10f;
            pickUpGoldAudioSource.Play();
            goldRenderer.enabled = false;
            Destroy(gameObject, 0.5f);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Exit : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) 
        {
            Application.Quit();
        }
    }
}

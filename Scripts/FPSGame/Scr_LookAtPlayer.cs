using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_LookAtPlayer : MonoBehaviour
{
    [SerializeField] Transform target;

    Vector3 lookAtVector;
    void Update()
    {
        LoockAtPlayer();
    }
    void LoockAtPlayer()
    {
        lookAtVector.Set(target.position.x, transform.position.y, target.position.z);
        transform.LookAt(lookAtVector);
    }
}

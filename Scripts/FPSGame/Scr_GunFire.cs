using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_GunFire : MonoBehaviour
{
    [SerializeField] GameObject muzzleFlush;
    [SerializeField] AudioSource fireSound;
    [SerializeField] AudioSource emptySound;
    [SerializeField] float damage;
    [SerializeField] float distanceToTarget;
    [SerializeField] Animator gunAnimator;

    bool isFiring = false;
    RaycastHit hit;

    void Start()
    {
        muzzleFlush.SetActive(false);
        isFiring = false;
    }
    void Update()
    {
        GunFire();
    }
    void GunFire()
    {
        if (Input.GetButtonDown("Fire1") && !isFiring && Scr_GlobalStats.haveGun)
        {
            if (Scr_GlobalStats.ammoAmount > 0)
                StartCoroutine(FiringGun());
            else
                StartCoroutine(EmptyGun());
        }
    }
    IEnumerator FiringGun()
    {
        isFiring = true;
        Scr_GlobalStats.ammoAmount--;
        gunAnimator.Play("GunFire");
        muzzleFlush.SetActive(true);
        fireSound.Play();

        HitEnemy();

        yield return new WaitForSeconds(0.05f);

        muzzleFlush.SetActive(false);

        yield return new WaitForSeconds(0.25f);
        gunAnimator.Play("Default");
        isFiring = false;
    }
    IEnumerator EmptyGun()
    {
        isFiring = true;
        emptySound.Play();
        yield return new WaitForSeconds(0.3f);
        isFiring = false;
    }
    void HitEnemy()
    {
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit))
        {
            distanceToTarget = hit.distance;
            hit.transform.SendMessage("DamageEnemy", damage, SendMessageOptions.DontRequireReceiver);
        }
    }
}

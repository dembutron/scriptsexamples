using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scr_PickUpAmmo : MonoBehaviour
{
    [SerializeField] AudioSource pickUpAudioSource;
    [SerializeField] GameObject pickUpDisplay;

    MeshRenderer ammoBoxRenderer;
    void Start()
    {
        ammoBoxRenderer = GetComponent<MeshRenderer>();
    }
    void Update()
    {
        RotateAmmo();
    }
    void OnTriggerEnter(Collider other)
    {
        PickUpGun(other);
    }
    void RotateAmmo()
    {
        transform.Rotate(0f, 0f, -100f * Time.deltaTime);
    }
    void PickUpGun(Collider collidingObject)
    {
        if (collidingObject.tag == "Player")
        {
            GetComponent<BoxCollider>().enabled = false;
            pickUpDisplay.SetActive(false);
            pickUpDisplay.GetComponent<Text>().text = "Ammo Box";
            pickUpDisplay.SetActive(true);
            Scr_GlobalStats.ammoAmount += 10;
            pickUpAudioSource.Play();
            ammoBoxRenderer.enabled = false;
            Destroy(gameObject, 0.5f);
        }
    }
}

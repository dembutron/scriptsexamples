using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scr_PickUpGun : MonoBehaviour
{
    [SerializeField] AudioSource pickUpAudioSource;
    [SerializeField] GameObject fakeGun;
    [SerializeField] GameObject realGun;
    [SerializeField] GameObject pickUpDisplay;
    void Update()
    {
        RotateGun();
    }
    void OnTriggerEnter(Collider other)
    {
        PickUpGun(other);
    }
    void PickUpGun(Collider collidingObject)
    {
        if (collidingObject.tag == "Player")
        {
            Scr_GlobalStats.haveGun = true;

            GetComponent<BoxCollider>().enabled = false;
            pickUpDisplay.SetActive(false);
            pickUpDisplay.GetComponent<Text>().text = "Handgun";
            pickUpDisplay.SetActive(true);
            pickUpAudioSource.Play();
            fakeGun.SetActive(false);
            realGun.SetActive(true);
            Destroy(gameObject, 0.5f);
        }
    }
    void RotateGun()
    {
        fakeGun.transform.Rotate(0f, -100f * Time.deltaTime, 0f);
    }
}

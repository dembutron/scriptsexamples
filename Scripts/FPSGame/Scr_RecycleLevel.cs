using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scr_RecycleLevel : MonoBehaviour
{
    [SerializeField] GameObject gameOverDisplay;
    void Start()
    {
        gameOverDisplay.SetActive(false);

        Scr_GlobalStats.livesValue--;

        if (Scr_GlobalStats.livesValue > 0)
            SceneManager.LoadScene(1);
        else
            gameOverDisplay.SetActive(true);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_EnemyDeath : MonoBehaviour
{
    [SerializeField] GameObject enemyAI;
    [SerializeField] float enemyHealth;
    [SerializeField] bool isDead;

    void Start()
    {
        enemyHealth = 20f;
        isDead = false;
    }
    void Update()
    {
        DeathOfEnemy();
    }
    void DeathOfEnemy()
    {
        if (enemyHealth <= 0 && !isDead)
        {
            isDead = true;
            enemyAI.SetActive(false);
            Destroy(gameObject, 1f);
        }
    }
    void DamageEnemy(float damageAmount)
    {
        enemyHealth -= damageAmount;
    }
}

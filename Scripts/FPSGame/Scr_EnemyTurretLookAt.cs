using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_EnemyTurretLookAt : MonoBehaviour
{
    [SerializeField] Transform target;

    public float rotationSpeed = 2;

    Vector3 lookAtVector;
    Quaternion lookAtRotation;

    void Update()
    {
        LookAtTarget();
    }
    void LookAtTarget()
    {
        lookAtVector = target.position - transform.position;
        lookAtVector.y = 0f;

        lookAtRotation = Quaternion.LookRotation(lookAtVector);

        transform.rotation = Quaternion.Lerp(transform.rotation, lookAtRotation, Time.deltaTime * rotationSpeed);
    }
}
